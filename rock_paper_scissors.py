import time 
import random

from click._compat import raw_input

"""
CHOICE <=> SELECTION 
#1 rock
#2 paper                         
#3 Scissor
"""
# Score System
CPU=0
YOU=0
#game start
re = raw_input("Start Game (Yes/No)")
#Program begins here
while (re=='y') or (re=='Y') or (re=='yes') or (re=='Yes'):
    a = random.randint(1,3)
    print ("\n")
    #print ("Comp" + str(a))
    b = int(raw_input("Enter Rock(1) Paper (2) Scissor (3): \n"))
#CONDITIONAL STATEMENTS
    if (a==1 and b==1) or(a==2 and b==2) or(a==3 and b==3):
        print ("\n TIE! \n")
    elif (a==1 and b==2):
        print ("CPU: Rock \n You:Paper \n YOU WIN!!")
        YOU=YOU+1 #score
    elif (a==1 and b==3):
        print ("CPU: Rock \n You:Scissor \n CPU WIN!!")
        CPU=CPU+1#score              
    elif (a==2 and b==1):
        print ("CPU: Paper \n You:Rock \n CPU WIN!!")
        CPU=CPU+1#score
    elif (a==2 and b==3):
        print ("CPU:Paper \n You:Scissor \n YOU WIN!!")
        YOU=YOU+1#score           
    elif (a==3 and b==1):
        print ("CPU:Scissor \n You:Rock \n YOU WIN!!")
        YOU=YOU+1#score
    elif (a==3 and b==2):
        print ("CPU: Scissor \n You:Paper \n CPU WIN!!")
        CPU=CPU+1#score
    print ("Player -",YOU, "CPU -",CPU)
    print ("\n")
    re = raw_input('Again? (Y/N)')
print ("\n")
print ("Thanks for playing!")
time.sleep(1) #OPTIONAL
exit()